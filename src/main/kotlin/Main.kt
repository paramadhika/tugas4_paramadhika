import java.lang.NumberFormatException

fun penilaian(nilai: Double, ratarata: Double){     //Fungsi ini menerima nilai inputan user -> nilai
    if(nilai < 0.0 || nilai > 100){                 //dan nilai kkm -> ratarata
        println("Error")                            //Fungsi ini digunakan untuk menetukan dan mencetak
    }                                               //apakah nilai inputan user sudah berada diatas/bawah
    else if(nilai < ratarata){                      //Nilai KKM
        println("Dibawah KKM")
    }
    else if (nilai > ratarata){
        println(" Diatas KKM")
    }
    else if (nilai == ratarata){
        println(" KKM")
    }
}

fun main(args: Array<String>) {
    var input: String                       //variabel untuk menyimpan sementara nilai inputan user
    var pembanding: Double                  //variabel yang digunakan untuk menyimpan nilai kkm sebagai pembanding
    var penentu: Double                     //variabel yang digunakan untuk menyimpan nilai inputan user sebagai penentu
    var i : Int = 0                         //variabel untuk melakukan traversal indeks arraylist
    var total: Double = 0.0

    val hasilUjian = ArrayList<Double>()    //ArrayList untuk menyimpan nilai hasil ujian inputan user

    /* lamda yang digunakan untuk menghitung rata-rata nilai semua mata pelajaran inputan user
       dengan menerima variabel total -> r yang kemudian dibagi dengan banyaknya nilai ujian dari
       arraylist hasilUjian */
    val ratarata = {r: Double -> r /hasilUjian.size}

    val kkm: ArrayList<ArrayList<Any>> =
        arrayListOf(arrayListOf("Matematika", 74.0),
            arrayListOf("Biologi", 78.0),
            arrayListOf("Fisika", 70.0),
            arrayListOf("Kimia", 72.0)
        )

    println("\t*Sistem Penilaian Siswa MIPA*")
    println("Nilai KKM dari Pelajaran Kelas MIPA :")    //proses untuk mencetak data nilai dari arraylist kkm
        kkm.forEach {
            println(" $it ")
        }

    println("\t\t========= Input Data Siswa =========")
    print("Masukkan Nama : ")                           //Proses Menginputkan nama siswa
    val nama = readLine()
    for(element in kkm){                                //proses menginputkan nilai dari seorang siswa sebanyak pelajaran pada kkm
            try{
                print("Masukkan Nilai ${element.get(0)} --> ")  //proses try/catch yang digunakan untuk memastikan inputan user berupa angka
                input = readLine()!!.toDouble().toString()
            }catch (e: NumberFormatException){
                println("!!!Peringatan Masukkan Angka!!!")
                print("Masukkan Nilai ${element.get(0)} --> ")
                input = readLine().toString()
            }
        val nilai = input.toDouble()                            //proses konversi data var. input menjadi double yang kemudian disimpan ke var. nilai
        hasilUjian.add(nilai)                                   //memasukkan data var. nilai ke arraylist
    }
    println("\n\n\t\t========= Hasil Penilaian Data Siswa =========")   //Proses untuk mencetak data siswa yang diinputkan sebelumnya
    println("Nama : $nama")
    for(element in kkm){                                                //perulangan dilakukan sebanyak data pelajaran arraylist kkm
        pembanding = element.get(1).toString().toDouble()               //proses untuk mengambil data arraylist kkm indeks[0][1] yang menyimpan
        penentu = hasilUjian[i]                                         //nilai kkm masing-masing pelajaran kedalam var. pembanding
        total = total + hasilUjian[i]                                   //var. total digunakan untuk menyimpan hasil jumlah dari nilai siswa
        print("Subject ${element.get(0)} --> ")
        penilaian(penentu, pembanding)                                  //pemanggilan function penilaian
        i++
    }
    println()
    val rerata = ratarata(total)                                        //pemanggilan lamda ratarata()
    print("Nilai Rata-Rata Siswa diatas adalah $rerata")                //mencetak nilai rata-rata siswa
}